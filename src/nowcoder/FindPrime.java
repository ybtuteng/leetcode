package nowcoder;

/**
 * Created by ybtuteng on 2017/8/18.
 */
public class FindPrime {
    public static void main(String[] args){
        int i = 10000007;
        int[] num = new int[i + 1];
        for(int j = 2; j < (i / Math.sqrt(i)) + 1; j++) {
            if(num[j] == 1)
                continue;
            int p = 2;
            while (j * p < i) {
                num[j * p] = 1;
                p++;
            }
        }
        for(int in = 0; in < 1000; in++){
            if(num[in] == 0)
                System.out.println(in);
        }
    }
}
