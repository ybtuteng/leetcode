package nowcoder;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 网易的棋盘问题
 * Created by ybtuteng on 2017/8/15.
 */
public class Chess {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int[] x = new int[length];
        for(int i = 0; i < length; i++){
            x[i] = sc.nextInt();
        }
        int[] y = new int[length];
        for(int i = 0; i < length; i++){
            y[i] = sc.nextInt();
        }
        int[] res = new int[length];
        int[] distance = new int[length * length];
        for(int n = 1; n < length + 1; n++) {
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    int[] temp = new int[length];
                    for (int z = 0; z < length; z++) {
                        temp[z] = Math.abs(x[z] - x[i]) + Math.abs(y[z] - y[j]);
                    }
                    Arrays.sort(temp);
                    for(int k = 0; k < n; k++){
                        System.out.println(i * length + j + " and " + k);
                        distance[i * length + j] += temp[k];
                    }
                }
            }
            int min = Integer.MAX_VALUE;
            for(int m = 0; m < length * length; m++){
                if(distance[m] < min)
                    min = distance[m];
            }
            res[n - 1] = min;
            distance = new int[length * length];
        }
        for(int i = 0; i < length - 1; i++){
            System.out.print(res[i] + " ");
        }
        System.out.print(res[length - 1]);

    }
}
