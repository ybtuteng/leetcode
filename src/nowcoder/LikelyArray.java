package nowcoder;

import java.util.Scanner;

/**
 * Created by ybtuteng on 2017/8/15.
 */
public class LikelyArray {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int[][] element = new int[k][k];
        for(int i = 0; i < k; i++){
            for(int j = 0; j < k; j++){
                if(i <= j || (i+1) % (j+1) != 0){
                    element[i][j] = 1;
                }
            }
        }
        int[][] sum = new int[k][k];
        for(int i = 0; i < k; i++) {
            System.arraycopy(element[i], 0, sum[i], 0, k);
        }
        for(int i = 2; i < n; i++){
            int[][] temp = new int[k][k];
            for(int d = 0; d < k; d++) {
                System.arraycopy(sum[d], 0, temp[d], 0, k);
            }
            for(int j = 0; j < k; j++){
                for(int m = 0; m < k; m++){
                    int sb = 0;
                    for(int z = 0; z < k; z++) {
                        sb += (temp[j][z] * element[z][m]);
                    }
                    sum[j][m] = sb;
                }
            }
        }
        int res = 0;
        for(int i = 0; i < k; i++){
            for(int j = 0; j < k; j++){
                res += sum[i][j];
            }
        }
        System.out.print(res);
    }
}
