package leetcode;

/**
 * 621
 * Created by ybtuteng on 2017/7/9.
 */
public class TaskSheduler {
    public int leastInterval(char[] tasks, int n) {
        int[] taskCounts = new int[26];
        for(int i = 0; i < tasks.length; i++){
            taskCounts[tasks[i] - 'A']++;
        }
        return getLeastNum(taskCounts, n);
    }

    public int getLeastNum(int[] taskCounts, int n){
        int varities = 0;
        int minCounts = Integer.MAX_VALUE;
        int maxCounts = Integer.MIN_VALUE;
        for(int i = 0; i < taskCounts.length; i++){
            if(taskCounts[i] != 0){
                varities++;
                if(taskCounts[i] < minCounts)
                    minCounts = taskCounts[i];
                if(taskCounts[i] > maxCounts)
                    maxCounts = taskCounts[i];
            }
        }
        if(varities == 0)
            return 0;
        if(maxCounts == 1)
            return varities;

        if(varities >= (n + 1)){
            milusMin(taskCounts, minCounts);
            return varities * minCounts + getLeastNum(taskCounts, n);
        }else{
            if(!milusMin(taskCounts, minCounts))
                return (n+1) * minCounts + getLeastNum(taskCounts, n);
            else
                return (n+1) * minCounts + n - varities;
        }
    }

    public boolean milusMin(int[] taskCounts, int minCounts){
        boolean isEmpty = true;
        for(int i = 0; i < taskCounts.length; i++){
            if(taskCounts[i] != 0)
                taskCounts[i] -= (minCounts - 1);
            if(taskCounts[i] != 0)
                isEmpty = false;
        }
        return isEmpty;
    }
}
