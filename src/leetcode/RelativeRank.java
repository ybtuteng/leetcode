package leetcode;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by ybtuteng on 2017/4/25.
 */
public class RelativeRank {
    //我的做法
    public String[] findRelativeRanks(int[] nums) {
        int[] temp = nums.clone();
        Arrays.sort(temp);
        HashMap<Integer,Integer> rank = new HashMap<>();
        for(int i = 0; i < nums.length; i++){
            rank.put(temp[i],nums.length - i);
        }

        String[] res = new String[nums.length];
        for(int i = 0; i < nums.length; i++){
            res[i] = rank.get(nums[i]) + "";
            if(res[i].equals("1"))
                res[i] = "Gold Medal";
            if(res[i].equals("2"))
                res[i] = "Silver Medal";
            if(res[i].equals("3"))
                res[i] = "Bronze Medal";
        }
        return res;
    }

    //大神做法
    public String[] findRelativeRanks1(int[] nums) {
        int[][] pair = new int[nums.length][2];

        for (int i = 0; i < nums.length; i++) {
            pair[i][0] = nums[i];
            pair[i][1] = i;
        }

        Arrays.sort(pair, (a, b) -> (b[0] - a[0]));//这里居然用lambda写了个comparator，使得sort排序可以从大到小排，并且可以排二维数组，精妙啊

        String[] result = new String[nums.length];

        for (int i = 0; i < nums.length; i++) {
            if (i == 0) {
                result[pair[i][1]] = "Gold Medal";
            }
            else if (i == 1) {
                result[pair[i][1]] = "Silver Medal";
            }
            else if (i == 2) {
                result[pair[i][1]] = "Bronze Medal";
            }
            else {
                result[pair[i][1]] = (i + 1) + "";
            }
        }

        return result;
    }
}
