package leetcode;

/**
 * Created by ybtuteng on 2017/6/5.
 * 242
 */
public class ValidAnagram {
    //我的做法基本与大神做法一样，就是一个串在字母对应的位置+1，另一个在对应位置-1，若最后数组全为0则返回true
    public boolean isAnagram(String s, String t) {
        if(s.length() != t.length())
            return false;
        int[] nums = new int[26];
        char[] temp1 = s.toCharArray();
        char[] temp2 = t.toCharArray();
        for(int i = 0; i < temp1.length; i++){
            nums[temp1[i] - 'a']++;
            nums[temp2[i] - 'a']--;
        }
        for(int num:nums){
            if(num != 0)
                return false;
        }
        return true;
    }

}
