package leetcode;

/**
 * Created by ybtuteng on 2017/5/26.
 */
public class SameTree {

    //我的办法，利用前序和中序的序列化来唯一确定一个树，通过比较两者的序列就可以知道是否一致了，其实这多了很多不必要的步骤
    public static boolean isSameTree(TreeNode p, TreeNode q) {
        StringBuilder sbp = new StringBuilder();
        StringBuilder sbq = new StringBuilder();
        inOrder(p,sbp);
        preOrder(p,sbp);
        inOrder(q,sbq);
        preOrder(q,sbq);
        if(sbp.toString().equals(sbq.toString())){
            return true;
        }else{
            return false;
        }
    }

    public static void inOrder(TreeNode node, StringBuilder sb){
        if(node == null){
            sb.append("#$");
            return;
        }
        inOrder(node.left,sb);
        sb.append(node.val + "$");
        inOrder(node.right,sb);
    }

    public static void preOrder(TreeNode node, StringBuilder sb) {
        if (node == null) {
            sb.append("#$");
            return;
        }
        sb.append(node.val + "$");
        preOrder(node.left, sb);
        preOrder(node.right, sb);
    }

    //大神方法
    public static boolean isSameTree1(TreeNode p, TreeNode q){
        if(p == null && q == null)
            return true;
        return p != null && q != null && p.val == q.val && isSameTree1(p.left,q.left) && isSameTree1(p.right,q.right);
    }

}
