package leetcode;

import java.util.HashMap;
import java.util.LinkedList;


/**
 * Created by ybtuteng on 2017/4/5.
 */
public class KeyboardRow500 {
    public String[] findWords(String[] words) {
        String[] keybord = {"qwertyuiop","asdfghjkl","zxcvbnm"};
        HashMap<Character,Integer> first = new HashMap<>();
        for(int i = 0; i < keybord.length; i++){
            for(char letter: keybord[i].toCharArray()){
                first.put(letter,i);
            }
        }

        LinkedList<String> results = new LinkedList<String>();
        for(String word:words){
            if(word.equals("")) continue;
            int index = first.get(word.toLowerCase().charAt(0));
            for(char letter:word.toLowerCase().toCharArray()){
                if(first.get(letter) != index) {
                    index = -1;
                    break;
                }
            }
            if(index != -1)
                results.add(word);
        }

        return results.toArray(new String[0]);

    }
}
