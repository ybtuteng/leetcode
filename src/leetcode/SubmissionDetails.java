package leetcode;

import java.util.HashMap;

public class SubmissionDetails {
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        if(inorder.length == 0 || postorder.length == 0 || inorder.length != postorder.length)
            return null;
        HashMap<Integer,Integer> map = new HashMap<>();
        int len = inorder.length;
        for(int i = 0; i < inorder.length; i++){
            map.put(inorder[i],i);
        }
        return built(inorder, 0, len - 1, postorder, 0, len -1, map);
        
    }
    
    public TreeNode built(int[] inorder,int is, int ie, int[] postorder, int ps, int pe, HashMap<Integer,Integer> map){
        if(pe < ps || ie < is) return null;
        int rv = postorder[pe];
        TreeNode root = new TreeNode(rv);
        int ri = map.get(rv);
        TreeNode leftNode = built(inorder, is, ri - 1, postorder, ps, ps + ri - is - 1,map);
        TreeNode rightNode = built(inorder, ri + 1, ie, postorder, pe + ri - ie,pe - 1, map);
        root.left = leftNode;
        root.right = rightNode;
        return root;
    }
}
