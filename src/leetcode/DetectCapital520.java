package leetcode;

/**
 * Created by ybtuteng on 2017/4/17.
 */
public class DetectCapital520 {
    public boolean detectCapitalUse(String word) {
        if(word.length() < 2)
            return true;
        if(Character.isUpperCase(word.charAt(0))) {
            for (int i = 1; i < word.length() - 1; i++) {
                if ((word.charAt(i) - 91) * (word.charAt(i + 1) - 91) < 0)
                    return false;
            }
            return true;
        }else{
            for (int i = 0; i < word.length(); i++) {
                if (Character.isUpperCase(word.charAt(i)))
                    return false;
            }
            return true;
        }
    }

    public boolean detectCapitalUse2(String word) {
        if (word.length() < 2) return true;
        if (word.toUpperCase().equals(word)) return true;
        if (word.substring(1).toLowerCase().equals(word.substring(1))) return true;
        return false;
    }

    public boolean detectCapitalUse3(String word) {
        return word.matches("[A-Z]+|[a-z]+|[A-Z][a-z]+");
    }
}
