package leetcode;

/**
 * Created by ybtuteng on 2017/4/25.
 * 283
 */
public class MoveZeros {
    //我的做法
    public void moveZeroes(int[] nums) {
        int index = 0;
        for(int i = 0; i < nums.length; i++){
            if(nums[i] != 0)
                nums[index++] = nums[i];
        }
        for(int i = index; i < nums.length; i++){
            nums[i] = 0;
        }
    }

    //大神做法
    public void moveZeroes1(int[] nums) {

        int j = 0;
        for(int i = 0; i < nums.length; i++) {
            if(nums[i] != 0) {
                int temp = nums[j];
                nums[j] = nums[i];
                nums[i] = temp;
                j++;
            }
        }
    }
}
