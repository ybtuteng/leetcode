package leetcode;

/**
 * Created by ybtuteng on 2017/4/6.
 */
public class ReverseString344 {
    public String reverseString(String s) {
        return new StringBuilder(s).reverse().toString();
    }
}
