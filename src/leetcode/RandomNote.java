package leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ybtuteng on 2017/5/4.
 */
public class RandomNote {
    public boolean canConstruct(String ransomNote, String magazine) {
        Map<Character,Integer> magM= new HashMap<>();
        for(Character ch:magazine.toCharArray()){
            int count = magM.getOrDefault(ch,0) + 1;
            magM.put(ch,count);
        }

        for(Character ch:ransomNote.toCharArray()){
            int count = magM.getOrDefault(ch,0) - 1;
            if(count < 0)
                return false;
            magM.put(ch,count);
        }
        return true;
    }
}
