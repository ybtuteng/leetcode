package leetcode;

import java.util.Collections;
import java.util.PriorityQueue;

/**
 * Created by tuteng on 17-7-20.
 */
public class TaskScheduler {
    public int leastInterval(char[] tasks, int n) {
        int[] counts = new int[26];
        for(int i = 0; i < tasks.length; i++){
            counts[tasks[i] - 'A']++;
        }
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(26, Collections.reverseOrder());
        for(int i: counts){
            if(i != 0){
                priorityQueue.add(i);
            }
        }
        int step = 0;
        int total = 0;
        int[] buffer = new int[n + 1];
        while(!priorityQueue.isEmpty()){
            for(int i = 0; i < n + 1; i++) {
                step++;
                if(!priorityQueue.isEmpty()) {
                    buffer[i] = priorityQueue.poll() - 1;
                    total += buffer[i];
                }
                if(buffer[(i+1)%(n+1)] != 0){
                    priorityQueue.add(buffer[(i+1)%(n+1)]);
                    total -= buffer[(i+1)%(n+1)];
                }
                if(total == 0 && priorityQueue.isEmpty()){
                    break;
                }
            }
        }
        return step;
    }
}
