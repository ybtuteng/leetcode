package leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ybtuteng on 2017/4/6.
 */
public class FizzBuzz412 {
    public List<String> fizzBuzz(int n) {
        List<String> list = new ArrayList<>();
        for(int i = 1; i <= n; i++){
            list.add((i % 3 == 0)?(i % 5 == 0?"FizzBuzz":"Fizz"):(i % 5 == 0?"Buzz":i + ""));
        }
        return list;
    }
}
