package leetcode;

import java.util.Scanner;

/**
 * Created by ybtuteng on 2017/5/7.
 */
public class NumberOfIsland {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()) {
            int row = sc.nextInt();
            int line = sc.nextInt();
            int[][] grid = new int[row][line];
            String temp = sc.nextLine();
            for(int i = 0; i < row;i++){
                temp = sc.nextLine();
                for(int j = 0; j < line;j++){
                    grid[i][j] = Integer.parseInt(temp.charAt(j) + "");
                }
            }
            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    System.out.print(grid[i][j]);
                }
                System.out.println();
            }

            int count = 0;

            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    if (grid[i][j] == 1) {
                        count++;
                        clearLand(grid, i, j);
                    }
                }
            }
            System.out.println(count);
        }

    }

    public static void clearLand(int[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid[i].length || grid[i][j] == 0)
            return;

        grid[i][j] = 0;
        clearLand(grid, i+1, j);
        clearLand(grid, i-1, j);
        clearLand(grid, i, j+1);
        clearLand(grid, i, j-1);
        return;
    }
}
