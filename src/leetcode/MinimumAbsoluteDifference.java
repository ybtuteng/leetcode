package leetcode;

/**
 * Created by ybtuteng on 2017/4/26.
 * 530
 */
public class MinimumAbsoluteDifference {
    //我的做法
    public int getMinimumDifference(TreeNode root) {
        int[] res = new int[2];
        res[0] = Integer.MAX_VALUE;
        res[1] = -1;
        inOrder(root,res);
        return res[0];

    }

    public static void inOrder(TreeNode root,int[] res){
        if(root == null)
            return;
        inOrder(root.left,res);
        if(res[1] != -1)
            res[0] = Math.min(res[0], root.val - res[1]);
        res[1] = root.val;
        inOrder(root.right,res);
    }

    //大神做法和我基本一样

}
