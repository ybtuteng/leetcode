package leetcode;

/**
 * Created by ybtuteng on 2017/4/25.
 * 563
 */
public class BinaryTreeTilt {
    //我的方法，递归解决
    public int findTilt(TreeNode root) {
        if(root == null)
            return 0;
        int rootTilt = Math.abs(getTilt(root.left) - getTilt(root.right));
        return findTilt(root.left) + findTilt(root.right) + rootTilt;
    }

    public int getTilt(TreeNode root){
        if(root == null)
            return 0;
        if(root.left == null && root.right == null)
            return root.val;
        return getTilt(root.left) + getTilt(root.right) + root.val;
    }

    int tilt = 0;

    //大神方法，后序遍历+全局变量。我对遍历的认识还不够深刻
    public int findTilt2(TreeNode root) {
        postorder(root);
        return tilt;
    }

    public int postorder(TreeNode root) {
        if (root == null) return 0;
        int leftSum = postorder(root.left);
        int rightSum = postorder(root.right);
        tilt += Math.abs(leftSum - rightSum);
        return leftSum + rightSum + root.val;
    }

}
