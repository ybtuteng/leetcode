package leetcode;

/**
 * Created by ybtuteng on 2017/4/25.
 * 492
 */
public class ConstructTheRectangle {
    //我的做法
    public int[] constructRectangle(int area) {
        int temp = (int)Math.sqrt(area);
        int l = temp;
        int w = temp;
        while(l * w != area){
            if(l * w > area)
                w--;
            else
                l++;
        }
        return new int[]{l,w};
    }

    //大神做法
    public int[] constructRectangle2(int area) {
        int w = (int)Math.sqrt(area);
        while (area%w!=0) w--;
        return new int[]{area/w, w};
    }
}
