package leetcode;

/**
 * Created by ybtuteng on 2017/5/4.
 * 453
 */
public class MinMove {
    public static void main(String[] args){
        System.out.println(minMoves(new int[]{0,100,200}));
    }

    //简单的数学推导
    public static int minMoves(int[] nums) {
        if(nums.length == 1)
            return 0;
        int min = nums[0];
        int sum = 0;
        for(int i = 0; i < nums.length;i++){
            if(nums[i] < min)
                min = nums[i];
            sum += nums[i];
        }
	return sum - nums.length * min;

    }
}
