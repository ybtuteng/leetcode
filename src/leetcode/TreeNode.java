package leetcode;

/**
 * Created by ybtuteng on 2017/4/21.
 */
public class TreeNode {
    int val;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;
    }
}
