package leetcode;

import java.util.HashMap;

/**
 * Created by ybtuteng on 2017/5/6.
 * 387
 */
public class FirstUnique {

    public int firstUniqChar(String s) {
        char[] ch = s.toCharArray();
        HashMap<Character,Integer> counts = new HashMap<>();
        for(int i = 0; i < ch.length;i++) {
            if(counts.containsKey(ch[i])) {
                int temp = counts.get(ch[i]) + 1;
                counts.put(ch[i], temp);
            }
            else
                counts.putIfAbsent(ch[i], 1);
        }
        for(int i = 0;i<ch.length;i++){
            if(counts.get(ch[i]) == 1)
                return i;
        }
        return -1;
    }
}
