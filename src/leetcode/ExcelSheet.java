package leetcode;

import java.util.HashMap;

/**
 * Created by ybtuteng on 2017/5/8.
 */
public class ExcelSheet {
    //我的方法
    public int titleToNumber(String s) {
        HashMap<Character,Integer> digit = new HashMap<>();
        for(int i = 0; i < 26; i++){
            digit.put((char)('A' + i),i+1);
        }
        char[] ch = s.toCharArray();
        int num = 0;
        for(int i = ch.length - 1; i >= 0; i--){
            num += digit.get(ch[i]) * Math.pow(26,(ch.length - i - 1));
        }
        return num;
    }

    //大神方法，类似于右移，经典
    public int titleToNumber2(String s) {
        int result = 0;
        for(int i = 0 ; i < s.length(); i++) {
            result = result * 26 + (s.charAt(i) - 'A' + 1);
        }
        return result;
    }
}
