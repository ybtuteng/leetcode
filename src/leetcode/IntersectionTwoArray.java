package leetcode;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by ybtuteng on 2017/5/6.
 * 349
 */
public class IntersectionTwoArray {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> num1 = new HashSet<Integer>();
        HashSet<Integer> num2 = new HashSet<>();
        for(int i = 0; i < nums1.length; i++){
            num1.add(nums1[i]);
        }
        for(int i = 0; i < nums2.length;i++){
            if(num1.contains(nums2[i]))
                num2.add(nums2[i]);
        }
        Iterator it = num2.iterator();
        int[] res = new int[num2.size()];
        int index = 0;
        while(it.hasNext()){
            res[index++] = (int)it.next();
        }
        //it.forEachRemaining(num->System.out.println(num));
        return res;
    }
}
