package leetcode;

import java.util.LinkedList;

/**
 * Created by ybtuteng on 2017/5/6.
 */
public class BestTimeStock {
    public int maxProfit(int[] prices) {
        LinkedList<Integer> queue = new LinkedList<>();
        int profit = 0;
        for(int i = 0; i < prices.length;i++){
            if(i < prices.length - 1 && prices[i + 1] > prices[i] ){
                queue.add(prices[i]);
            }else if(!queue.isEmpty()){
                profit += (prices[i] - queue.getFirst());
                queue.clear();
            }
        }
        return profit;
    }

    //大神做法
    public int maxProfit1(int[] prices) {
        int total = 0;
        for (int i=0; i< prices.length-1; i++) {
            if (prices[i+1]>prices[i]) total += prices[i+1]-prices[i];
        }

        return total;
    }
}
