package leetcode;

import java.util.Arrays;

/**
 * Created by ybtuteng on 2017/7/23.
 */
public class ValidTriangleNumber {
    //我的做法
    public int triangleNumber(int[] nums) {
        Arrays.sort(nums);
        int count = 0;
        for(int i = 0; i < nums.length; i++){
            for(int j = i + 1; j < nums.length; j++){
                for(int z = j + 1; j < nums.length; z++){
                    if(nums[z] < nums[i] + nums[j])
                        count++;
                    else
                        break;;
                }
            }
        }
        return count;
    }

    //大神做法，循环判断最大那个点，另外两个点r,l分别为数组的右端和左端，若两端满足条件，中间的全部满足条件，两端不断向里面逼近，总复杂度o(n^2)
    public static int triangleNumber2(int[] A) {
        Arrays.sort(A);
        int count = 0, n = A.length;
        for (int i=n-1;i>=2;i--) {
            int l = 0, r = i-1;
            while (l < r) {
                if (A[l] + A[r] > A[i]) {
                    count += r-l;
                    r--;
                }
                else l++;
            }
        }
        return count;
    }

}
