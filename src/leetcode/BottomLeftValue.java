package leetcode;

import java.util.ArrayDeque;

/**
 * Created by 涂腾 on 2017/6/27.
 */
public class BottomLeftValue {
    public int findBottomLeftValue(TreeNode root) {
        ArrayDeque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        TreeNode levelNode = root;
        TreeNode res= root;
        while(!queue.isEmpty()){
            TreeNode temp = queue.poll();
            if(temp.left != null){
                queue.add(temp.left);
            }
            if(temp.right != null){
                queue.add(temp.right);
            }
            if(levelNode == temp && !queue.isEmpty()){
                levelNode = queue.peekLast();
                res = queue.peek();
            }
        }
        return res.val;
    }
}
