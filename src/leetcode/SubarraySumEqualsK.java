package leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tuteng on 17-7-21.
 */
public class SubarraySumEqualsK {
    public int subarraySum(int[] nums, int k) {
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        int pre = 0;
        for(int i = 0; i < nums.length; i++){
            pre = pre + nums[i];
            if(map.containsKey(pre)){
                map.put(pre, map.get(pre) + 1);
            }else{
                map.put(pre, 1);
            }
        }

        int counts = 0;
        pre = 0;
        for(int i = 0; i < nums.length; i++){
            map.put(pre, map.get(pre) - 1);
            if(map.containsKey(k + pre)){
                counts += map.get(k + pre);
            }
            pre = pre + nums[i];
        }
        return counts;

    }

    //大神做法
    public int subarraySum2(int[] nums, int k) {
        int count = 0, sum = 0;
        HashMap < Integer, Integer > map = new HashMap < > ();
        map.put(0, 1);
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (map.containsKey(sum - k))
                count += map.get(sum - k);
            if(map.containsKey(sum)){
                map.put(sum, map.get(sum) + 1);
            }else{
                map.put(sum, 1);
            }
        }
        return count;
    }
}
