package leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ybtuteng on 2017/6/5.
 * 169
 */
public class MajorityElement {
    public int majorityElement(int[] nums) {
        Map<Integer,Integer> counts = new HashMap<>();
        int max = 0;
        int element = nums[0];
        for(int i = 0; i < nums.length; i++){
            if(counts.containsKey(nums[i]))
                counts.put(nums[i],counts.get(nums[i]) + 1);
            else
                counts.put(nums[i],1);
            if(counts.get(nums[i]) > max){
                max = counts.get(nums[i]);
                element = nums[i];
            }
        }
        return element;
    }

    //大神做法，相异元素的次数互相抵消，因为题目说了主元素一定存在，所以一定有足够的主元素去抵消掉所有的非主元素，何况还有非主元素与非主元素之间的抵消呢？
    public int majorityElement1(int[] num) {

        int major=num[0], count = 1;
        for(int i=1; i<num.length;i++){
            if(count==0){
                count++;
                major=num[i];
            }else if(major==num[i]){
                count++;
            }else count--;

        }
        return major;
    }

    //另类的做法，32位整数一位一位的看，每一位看是1多还是0多，就可以判断我们要找的那个数的那个位是1还是0，很新奇，虽然不够快
    public int majorityElement2(int[] num) {

        int ret = 0;

        for (int i = 0; i < 32; i++) {

            int ones = 0, zeros = 0;

            for (int j = 0; j < num.length; j++) {
                if ((num[j] & (1 << i)) != 0) {
                    ++ones;
                }
                else
                    ++zeros;
            }

            if (ones > zeros)
                ret |= (1 << i);
        }

        return ret;
    }
}
