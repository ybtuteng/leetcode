package leetcode;

public class FindTheDuplicateNumber {
    // 在一个数组中找到重复的那个数，要求不能用额外空间，不能改变数组本身，重复的数可能会重复多次（而不只是两次）
    // 非常巧妙的方法，从最后一个元素开始利用链表的方式往前找，快慢指针先找交点，再让快指针回归起点，快慢指针以同一速度再次前进，再次相交的点即为圈的入点
    public int findDuplicate(int[] nums) {
        int n = nums.length;
        int slow = nums[n -1] - 1;
        int fast = nums[slow] - 1;
        while(slow != fast){
            slow = nums[slow] - 1;
            fast = nums[nums[fast] - 1] - 1;
        }
        fast = nums[n -1] - 1;
        slow = nums[slow] - 1;
        while(fast != slow){
            slow = nums[slow] - 1;
            fast = nums[fast] - 1;
        }
        return fast + 1;
    }
}
