package leetcode;

import java.util.HashMap;
import java.util.Stack;

/**
 * Created by ybtuteng on 2017/4/15.
 */
public class NextGreaterElement496 {
    public int[] nextGreaterElement(int[] findNums, int[] nums) {
        Stack<Integer> stack = new Stack<>();
        HashMap<Integer,Integer> res = new HashMap<>();
        for(int i = nums.length - 1; i >= 0; i--){
            while(!stack.isEmpty() && nums[i] > stack.peek()){
                stack.pop();
            }
            if(stack.isEmpty())
                res.put(nums[i],-1);
            else
                res.put(nums[i],stack.peek());
            stack.push(nums[i]);
        }

        int[] results = new int[findNums.length];
        for(int i = 0; i < findNums.length; i++){
            results[i] = res.get(findNums[i]);
        }
        return results;
    }
}
