package leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ybtuteng on 2017/5/3.
 * 494
 */
public class FindTargetNum {
    //我的做法，动态规划，思路是建一个两倍sum的数组，前一半存放负数，后一半存整数，中间是0
    public int findTargetSumWays(int[] nums, int S) {
        int sum = 0;
        for(int i = 0; i < nums.length; i++){
            sum += nums[i];
        }
        if(S > sum || S < (-1 * sum))
            return 0;
        int[][] dp = new int[nums.length][2 * sum + 1];
        for(int i = 0; i < nums.length; i++){
            for(int j = 0; j < 2 * sum + 1; j++){
                if(i == 0){
                    dp[i][sum - nums[i]] += 1;
                    dp[i][sum + nums[i]] += 1;
                    break;
                }
                if(j < nums[i])
                    dp[i][j] = dp[i - 1][j + nums[i]];
                else if(j > 2 * sum - nums[i])
                    dp[i][j] = dp[i - 1][j - nums[i]];
                else
                    dp[i][j] = dp[i - 1][j + nums[i]] + dp[i - 1][j - nums[i]];
            }
        }

        return dp[nums.length - 1][S + sum];
    }

    //大神做法，利用如下结论，巧妙的避开了负数，P是正数序列，N是负数序列
    //sum(P) - sum(N) = target
    //sum(P) + sum(N) + sum(P) - sum(N) = target + sum(P) + sum(N)
                       //2 * sum(P) = target + sum(nums)
    public int findTargetSumWays2(int[] nums, int s) {
        int sum = 0;
        for (int n : nums)
            sum += n;
        return sum < s || (s + sum) % 2 > 0 ? 0 : subsetSum(nums, (s + sum) >>> 1);
    }

    public int subsetSum(int[] nums, int s) {
        int[] dp = new int[s + 1];
        dp[0] = 1;
        for (int n : nums)
            for (int i = s; i >= n; i--)
                dp[i] += dp[i - n];
        return dp[s];
    }

    //深度优先遍历，其实就是把中间结果记在了hashmap里，避免了直接递归产生的重复计算
    public int findTargetSumWays3(int[] nums, int S) {
        if (nums == null || nums.length == 0){
            return 0;
        }
        return helper(nums, 0, 0, S, new HashMap<>());
    }
    private int helper(int[] nums, int index, int sum, int S, Map<String, Integer> map){
        String encodeString = index + "->" + sum;
        if (map.containsKey(encodeString)){
            return map.get(encodeString);
        }
        if (index == nums.length){
            if (sum == S){
                return 1;
            }else {
                return 0;
            }
        }
        int curNum = nums[index];
        int add = helper(nums, index + 1, sum - curNum, S, map);
        int minus = helper(nums, index + 1, sum + curNum, S, map);
        map.put(encodeString, add + minus);
        return add + minus;
    }

    //和我的做法一个思想，但是更为简洁
    public int findTargetSumWays4(int[] nums, int s) {
        int sum = 0;
        for(int i: nums) sum+=i;
        if(s>sum || s<-sum) return 0;
        int[] dp = new int[2*sum+1];
        dp[0+sum] = 1;
        for(int i = 0; i<nums.length; i++){
            int[] next = new int[2*sum+1];
            for(int k = 0; k<2*sum+1; k++){
                if(dp[k]!=0){
                    next[k + nums[i]] += dp[k];
                    next[k - nums[i]] += dp[k];
                }
            }
            dp = next;
        }
        return dp[sum+s];
    }
}
