package src.leetcode;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by ybtuteng on 2017/7/23.
 */
public class ArrayNesting {
    //我的做法，主要是要把握住每个圈之间没有交点
    public int arrayNesting(int[] nums) {
        HashSet<Integer> process = new HashSet<>();
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if(process.contains(nums[i]))
                continue;
            int head = nums[i];
            int temp = 0;
            while (!process.contains(head)) {
                process.add(head);
                head = nums[head];
                temp++;
            }
            if(temp > max){
                max = temp;
            }
        }
        return max;
    }

    //大神做法，其实就是优化了空间复杂度，不用hashmap记录，直接将数组元素变为-1
    public int arrayNesting2(int[] a) {
        int maxsize = 0;
        for (int i = 0; i < a.length; i++) {
            int size = 0;
            for (int k = i; a[k] >= 0; size++) {
                int ak = a[k];
                a[k] = -1; // mark a[k] as visited;
                k = ak;
            }
            maxsize = Integer.max(maxsize, size);
        }

        return maxsize;
    }
}

