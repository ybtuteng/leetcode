package leetcode;

import java.util.Arrays;

/**
 * Created by ybtuteng on 2017/4/28.
 */
public class AssighCookie {
    public int findContentChildren(int[] g, int[] s) {
        Arrays.sort(g);
        Arrays.sort(s);
        String s11 = "ss";
        int i = 0;
        int j = 0;
        int counts = 0;
        while(i < g.length && j < s.length){
            if(s[j] >= g[i]){
                i++;
                j++;
                counts++;
            }else{
                j++;
            }
        }
        return counts;
    }
    public static void main(String[] args){
        System.out.println();
    }

    public static boolean judge(String word){
        if(word == null)
            return true;
        String[] words = word.split(" ");
        int start = 0;
        int tail = words.length - 1;
        while(start < tail){
            if(words[start] .equals(words[tail])){//应该是equals
                start++;
                tail--;
            }else{
                return false;
            }
        }
        return true;
    }
}
